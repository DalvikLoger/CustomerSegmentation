#Costumer Segmentation
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
from sklearn.cluster import KMeans
sns.set_theme() # pour modifier le thème

df = pd.read_csv('Test.csv',header=0 )

#Cleaning Dataset
df['Gender'] = df['Gender'].replace(to_replace=['Male','Female'],value=[1,0])
df['Ever_Married'] = df['Ever_Married'].replace(to_replace=['Yes','No'], value=[1,0])
df['Graduated'] = df['Graduated'].replace(to_replace=['Yes','No'], value=[1,0])
df = df.dropna(axis = 0, how = 'all', subset = ['Ever_Married'])
df = df.dropna(axis = 0, how = 'all', subset = ['Graduated'])
df = df.dropna(axis = 0, how = 'all', subset = ['Profession'])
df = df.dropna(axis = 0, how = 'all', subset = ['Var_1'])
df = df.drop(['Segmentation'],axis=1)
df['Work_Experience'] = df['Work_Experience'].fillna(0)
df['Family_Size'] = df['Family_Size'].fillna(0)
df['Spending_Score'] = df['Spending_Score'].replace(to_replace=['Low','Average','High'], value=[1,2,3])
df['Var_1'] = df['Var_1'].replace(to_replace=['Cat_1','Cat_2','Cat_3','Cat_4','Cat_5','Cat_6','Cat_7'], value=[1,2,3,4,5,6,7])

df = pd.get_dummies(df, columns=['Profession'])

from sklearn.cluster import AgglomerativeClustering

# Initialisation du classificateur CAH pour 4 clusters
cluster = AgglomerativeClustering(n_clusters = 4)

# Apprentissage des données 
cluster.fit(df)

# Calcul des labels du data set
labels = cluster.labels_

# Importation des packages nécessaires pour la CAH
from scipy.cluster.hierarchy import dendrogram, linkage

# Initialisaion de la figrue
plt.figure(figsize=(20, 10))

# Génération de la matrice des liens
Z = linkage(df, method = 'ward', metric = 'euclidean')

# Affichage du dendrogramme
plt.title("Dendrogramme CAH")
dendrogram(Z, labels = df.index, leaf_rotation = 90., color_threshold = 0)
plt.show()

from sklearn.metrics import silhouette_score

# Calcul du coefficient de silhouette
print(silhouette_score(df, labels, metric='sqeuclidean'))

df['Segmentation'] = labels
df['Segmentation'] = df['Segmentation'].map({0: 'A', 1: 'B', 2: 'C', 3: 'D'})