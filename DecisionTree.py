#Costumer Segmentation
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
from sklearn.cluster import KMeans
sns.set_theme() # pour modifier le thème

dt = pd.read_csv('Train_Agglomerative.csv')
dt['Gender'] = dt['Gender'].replace(to_replace=['Male','Female'],value=[1,0])
dt['Ever_Married'] = dt['Ever_Married'].replace(to_replace=['Yes','No'], value=[1,0])
dt['Graduated'] = dt['Graduated'].replace(to_replace=['Yes','No'], value=[1,0])
dt['Spending_Score'] = dt['Spending_Score'].replace(to_replace=['Low','Average','High'], value=[1,2,3])
dt['Var_1'] = dt['Var_1'].replace(to_replace=['Cat_1','Cat_2','Cat_3','Cat_4','Cat_5','Cat_6','Cat_7'], value=[1,2,3,4,5,6,7])
dt['Segmentation'] = dt['Segmentation'].replace(to_replace=['A','B','C','D'], value=[0,1,2,3])
dt = pd.get_dummies(dt, columns=['Profession'])

data = dt.drop(['Segmentation'], axis=1)
target = dt['Segmentation']

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=0.2, random_state=123)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

dt_clf = DecisionTreeClassifier(criterion ='entropy', max_depth=10, random_state=123)
dt_clf.fit(X_train, y_train)

y_pred = dt_clf.predict(X_test)

print(dt_clf.score(X_test, y_test))

pd.crosstab(y_test, y_pred, rownames=['Classe réelle'], colnames=['Classe prédite'])

from sklearn import tree

plt.subplots(figsize = (20, 10))
tree.plot_tree(dt_clf, fontsize=10)
plt.show()