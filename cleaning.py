#Costumer Segmentation
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
from sklearn.cluster import KMeans
sns.set_theme() # pour modifier le thème

df = pd.read_csv('Test.csv',header=0 )

#Cleaning Dataset
df['Gender'] = df['Gender'].replace(to_replace=['Male','Female'],value=[1,0])
df['Ever_Married'] = df['Ever_Married'].replace(to_replace=['Yes','No'], value=[1,0])
df['Graduated'] = df['Graduated'].replace(to_replace=['Yes','No'], value=[1,0])
df = df.dropna(axis = 0, how = 'all', subset = ['Ever_Married'])
df = df.dropna(axis = 0, how = 'all', subset = ['Graduated'])
df = df.dropna(axis = 0, how = 'all', subset = ['Profession'])
df = df.dropna(axis = 0, how = 'all', subset = ['Var_1'])
df = df.drop(['Segmentation'],axis=1)
df['Work_Experience'] = df['Work_Experience'].fillna(0)
df['Family_Size'] = df['Family_Size'].fillna(0)
print(df.isna().sum())
df['Spending_Score'] = df['Spending_Score'].replace(to_replace=['Low','Average','High'], value=[1,2,3])
df['Var_1'] = df['Var_1'].replace(to_replace=['Cat_1','Cat_2','Cat_3','Cat_4','Cat_5','Cat_6','Cat_7'], value=[1,2,3,4,5,6,7])

df = pd.get_dummies(df, columns=['Profession'])
print(df.head())